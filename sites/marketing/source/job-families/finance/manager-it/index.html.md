---
layout: job_family_page
title: "IT Management"
---

## Manager, IT

### Job Grade 

The Manager, IT is a [grade 8](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Responsibilities

* Build, scale and manage our IT Operations and Helpdesk teams to support our needs as distributed company
* Work with PeopleOps to enable smooth onboarding/offboarding of all team members, ensuring that
    * All hardware is available day one
    * Governance and deployment of software and SAAS is automated and aligned with GitLab policies
    * IT Onboarding/101 are regulary conducted, and enable team members to get comfortable working in their new tools
    * A clear channel of feedback to improve the efficacy of the above
* Be the IT Expert at GitLab - Come with Solutions, not a Practice of No
* Hold regular 1:1’s with all members of the IT Teams
* Triage and manage priorities of IT HelpDesk and Operations
* Represent the IT team in different company functions - be an advocate for modern, efficient, SIMPLE solutions
* Create and execute a plan to develop and mature our IT capabilities
* Regularly give IT group conversations
* Embed an asynchronous, distributed philosophy in everything you do
* Collaborate with all functions of the company to ensure IT needs are addressed
* This position reports to the Director of Business Operations

### Requirements

* 2+ years hands on experience in an IT Operations role, and experience managing or working service/support roles.
* 2+ years managing a team of 2 or more IT Administrators
* Experience working with leadership to execute on strategic IT investments, and creatively meeting the non-homogeneous needs of a technically savvy, specialized userbase in our R&D, go-to-market and G&A teams
* Contribute to and enable GitLab's operational strategy -- enabling distributed and asynch operations, and empowering our end users to take direct action
* Experience growing a team in a fast-paced, high-growth environment
* Demonstrably deep understanding of IT Operations and Service/Support needs in a high-growth technology company
* Ability to reason holistically about end-to-end Enterprise ecosystems: from online sales portals to marketing tools to GCP
* Hands on experience working with Python
* Experience building and maintaining automation and integration of enterprise IT SaaS platforms
* Experience building corporate IT knowledge bases and writing policies/processes
* Experience with open source tools
* Be passionate about DevOps, or its principles, especially in applying software engineering principles to IT Operations and Services
* Strong written and verbal communication skills
* Share and work in accordance with our values
* Must be able to work in alignment with Americas timezones
* Successful completion of a [background check](/handbook/people-group/code-of-conduct/#background-checks)
* [Leadership at GitLab](https://about.gitlab.com/company/team/structure/#management-group)
* Ability to use GitLab

## Performance Indicators (PI)

*  [New Hire Location Factor < 0.69](/handbook/business-ops/metrics/#new-hire-location-factor--069)
*  [% of team who self-classify as diverse](/handbook/business-ops/metrics/#percent--of-team-who-self-classify-as-diverse)
*  [Discretionary bonus per employee per month > 0.1](/handbook/business-ops/metrics/#discretionary-bonus-per-employee-per-month--01)
*  [Average Delivery Time of Laptop Machines < 21 days](/handbook/business-ops/metrics/#average-delivery-time-of-laptop-machines--21-days)
*  [Customer Satisfaction Survey (CSAT)](/handbook/business-ops/metrics/#customer-satisfaction-survey-csat)

## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on our [team page](/company/team).

* Selected candidates will be invited to schedule a [screening call](/handbook/hiring/#screening-call) with our Global Recruiters
* Next, candidates will be invited to schedule a first interview with our Director of Business Operations
* Next, candidates will be invited to schedule a second interview with our Head of People Operations
* Next, candidates will be invited to schedule one or more interviews with members of the BizOps team
* Finally, candidates may be asked to interview with our CFO or CEO

Additional details about our process can be found on our [hiring page](/handbook/hiring).

## Career Ladder 

The next step in the IT Management job family is to move to the leadership position which is not yet defined at GitLab. 
