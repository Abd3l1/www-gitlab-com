---
layout: handbook-page-toc
title: "Field-FYI Slack Channel"
description: "GitLab's official field announcements channel"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

The #field-fyi channel is the official field announcements channel. This is a read-only channel with permission levels restricted to Field and Company Leadership as well as Field Operations. The purpose of this channel is to pull news and announcements out of #sales where they can easily get buried and consolidate them in a streamlined channel. Anyone is free to react to and comment on posts in this channel, and field team members are encouraged to star it to stay up-to-date. For general field channels, see #sales and #customer-success.

## Best Practices

1. Even though the read only channel will help cut down on noise, we should still aim to post **no more than 3 updates in one day** to avoid saturating the team. (Some days we will have nothing to post, which is a plus!)
   1. If we've already hit the 3 post limit, please consider if your update can wait until the following business day before posting.
1. Feedback from the team already stresses that there is too much reading/block text. Slack should be a reprieve from this. We should aim to keep Slack updates as short and consumable as possible – 100 words or shorter as a best practice.
   1. Use numbered lists when possible to summarize key points.
   1. Link out to relevant documentation for any points that need additional context. Hit on the highlights in the update and let the documentation take it from there.
   1. Avoid having multiple link previews accompanying a post. Only keep the ones that are valuable to getting the point across. Delete all others. (Deleting all is okay, too.)
1. A peripheral goal of the field-fyi channel is to cut down on noise in other channels like #sales and #customer-success to make them more easily consumable. As a result, this channel should be the single source of Slack truth for important field announcements. Avoid cross posting to other channels when possible, as this will create more noise and defeat the purpose of the channel.

## Q&A
What are some examples of a post fit for #sales vs #field-fyi? 
1. Examples of #sales posts: General team questions, reminders about meetings, general chatter from team members, non time-sensitive updates/resources. 
1. Examples of #field-fyi posts: Updates from CRO, noteworthy process changes, important and time-sensitive resources, (enablement, sales ops, marketing etc.) time-sensitive People Ops information. 

What is the difference between #field-fyi and #cro channels?
1. The #cro channel is for announcements and communication specifically from/to McB. The #field-fyi channel will be for very important updates that do not come from McB. 
1. Alluding again to the format that we've taken with #company-fyi - Sid still has his #ceo channel that he uses to communicate with the team/field questions, but organization-wide posts go on #company-fyi. 

If someone has an update fitting for #field-fyi but doesn't have posting permissions, how can they get their news shared? 
1. If it's a one-off request, please reach out to #field-enablement-team or create a brief issue summarizing your requested announcement using the [Field announcements request process](/handbook/sales/field-communications/#requesting-field-announcements). 
1. If a team member needs the ability to share updates on a recurring basis, they can work directly with the [Field Enablement team](/handbook/sales/field-operations/field-enablement/) to discuss posting permissions. 
