---
layout: handbook-page-toc
title: Ask Me Anything
description: "Learn and ask questions at GitLab's Ask Me Anything (AMA) meetings"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Purpose

Ask Me Anything (AMA) meetings are arranged to allow team members the opportunity to learn from and ask questions of our [E-Group](https://about.gitlab.com/company/team/structure/#e-group), [Board of Directors](/handbook/board-meetings/#board-of-directors), and others groups who would like to host an AMA on a specific topic. AMAs can be arranged for many purposes, including the following:

- Allow team members to meet new leaders joining the company
- Clarify a company-wide initiative or change that requires more discussion
- Allow team members to learn more about current leaders
- Spark insightful discussions
- Initiate a retrospective discussion

### Who's Involved

AMA meetings typically include the whole company, and are usually hosted by a leader, team member, or a group of leaders.

They can be hosted by anyone who wants to host an AMA, but typically are hosted by one of the following:

- [GitLab E-Group Member](https://about.gitlab.com/company/team/?department=executive)
- Director + Leaders
- [Board Member](/handbook/board-meetings/#board-of-directors)
- [GitLab TMRG group](https://about.gitlab.com/company/culture/inclusion/erg-guide/)

Occasionally an AMA might be held for a smaller audience. It is encouraged for all AMAs to be open to the full company whenever possible. Occasionally an AMA may not make sense to be sent to the whole company. In those instances, it is still encouraged to add the calendar invitation to the GitLab Team Meetings Calendar as sometimes team members may want to join a call that may not be directly directly applicable to them. Whenever creating an AMA for a smaller group, be sure to invite the team directly in the calendar invite.

#### Scheduling an AMA

AMAs are generally coordinated, scheduled, and planned by the [Executive Business Administrator](https://about.gitlab.com/handbook/eba/) team when an E-Group member is the host. Anytime that an AMA is to be hosted by an executive, VP, or Director at GitLab, it should be arranged and scheduled by that functions EBA. AMAs of Board Members are scheduled by the Staff EBA to the CEO.

Other non-Executive AMAs are scheduled by the [People Operations](/handbook/people-group/#people-experience-vs-people-operations-core-responsibilities--response-timeline) team. These AMAs are typically a result of a change that impacts the company as a whole and may often be hosted by multiple leadership members in the company. Whenever there is an open slot in the Group Conversation calendar, the People Operations Team will reach out to the EBA team to create an AMA.

When scheduling an AMA, be mindful of timezones.  Generally it is best to have two AMAs, one for the Americas/EMEA time zone, and one for the APAC time zones.

### Setup

AMA meetings always start with an [agenda](https://docs.google.com/document/d/1-wrI4GB8N74O5AUmdnj916uhCbrL2adM8wbD_unrbac/edit?usp=sharing). Ensure that the agenda document is added to the calendar invite at the initial time it is sent to allow everyone to be involved. All questions and answers should be documented in the agenda to allow those who can not attend to catch up as well as to avoid team members talking over each other.

Whenever possible AMAs should be private or publicly [live streamed](/handbook/marketing/marketing-operations/youtube/#live-streaming). Please strive to remain [public by default](/handbook/values/#public-by-default) when hosting an AMA unless it is centered around a topic that is [not-public](/handbook/values/#not-public)

### Reverse Ask Me Anything

For details on hosting and scheduling a Reverse AMA, please visit the [Reverse AMA section](/handbook/people-group/women-in-sales-mentorship-pilot-program/#reverse-ask-me-anything) in the Handbook.
